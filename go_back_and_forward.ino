#include <SoftwareSerial.h>
SoftwareSerial EEBlue(A10, A11);

const int speedPin = 2;
const int dirPin = 3;
const int steerPWMPin = 4;
const int steerDirPin = 5;

const int ledLeft = 10;
const int ledCenter = 11;
const int ledRight = 12;

const int trigPin = 6;
const int echoPinLeft = 7;
const int echoPinCenter = 8;
const int echoPinRight = 9;

#define FORWARD 1
#define REVERSE 0

#define LEFT 0
#define RIGHT 1
#define STRAIGHT 2

int currentSteerDir = STRAIGHT;

#define SPEED 100
#define TURNING_SPEED 200
#define TURNING_DELAY 500

void setup() {
  EEBlue.begin(9600);
  EEBlue.setTimeout(200);
  pinMode(ledLeft, OUTPUT);
  pinMode(ledCenter, OUTPUT);
  pinMode (ledRight, OUTPUT);
  
  pinMode(speedPin, OUTPUT);
  pinMode(dirPin, OUTPUT);
  pinMode(steerPWMPin, OUTPUT);
  pinMode(steerDirPin, OUTPUT);

  pinMode(trigPin, OUTPUT);
  pinMode(echoPinLeft, INPUT);
  pinMode(echoPinCenter, INPUT);
  pinMode(echoPinRight, INPUT);

  // align steering
  alignSteering();
}

void loop(){

  int input, previousInput;

  if (EEBlue.available()) {
    input = EEBlue.parseInt();

    if (input == 8) {
      goForward();
    } else if (input == 2) {
      goReverse();
    } else if (input == 5) {
      brake();
      setLedHigh(ledCenter);
    } else if (input == 4) {
      steerLeft();
      setLedHigh(ledLeft);
    } else if (input == 6) {
      steerRight();
      setLedHigh(ledRight);
    } else if (input == 7) {
      alignSteering();
    }
  }

//  int leftDistance = getUSDistance(echoPinLeft);
//  int centerDistance = getUSDistance(echoPinCenter);
//  int rightDistance = getUSDistance(echoPinRight);
//
//  Serial.print("{\"left\":");
//  Serial.print(leftDistance);
//  Serial.print(",\"center\":");
//  Serial.print(centerDistance);
//  Serial.print(",\"right\":");
//  Serial.print(rightDistance);
//  Serial.println("}");
//
//  if (leftDistance != 0 && leftDistance < 40) {
//    brake();
//  }
//
//  if (centerDistance != 0 && centerDistance < 40) {
//    brake();
//  }
//
//  if (rightDistance != 0 && rightDistance < 40) {
//    brake();
//  }
}

void goForward() {
  digitalWrite(dirPin, HIGH);
  analogWrite(speedPin, SPEED);
}

void goReverse() {
  digitalWrite(dirPin, LOW);
  analogWrite(speedPin, SPEED);
}

void brake() {
  analogWrite(speedPin, 0);
}

void steerLeft() {

  if (currentSteerDir == LEFT) {
    return;
  }

  currentSteerDir = LEFT;
  
  digitalWrite(steerDirPin, LOW);
  analogWrite(steerPWMPin, TURNING_SPEED);
  delay(TURNING_DELAY);

  // stop applying turning force
  analogWrite(steerPWMPin, 0);
}

void steerRight() {

  if (currentSteerDir == RIGHT) {
    return;
  }

  currentSteerDir = RIGHT;
  
  digitalWrite(steerDirPin, HIGH);
  analogWrite(steerPWMPin, TURNING_SPEED);
  delay(TURNING_DELAY);

  // stop applying turning force
  analogWrite(steerPWMPin, 0);
}

void alignSteering() {
  if (currentSteerDir == LEFT) {
    // half turn right
    digitalWrite(steerDirPin, HIGH);
    analogWrite(steerPWMPin, TURNING_SPEED);
    delay(TURNING_DELAY / 2);
    analogWrite(steerPWMPin, 0);
  } else if (currentSteerDir == RIGHT) {
    // half turn left
    digitalWrite(steerDirPin, LOW);
    analogWrite(steerPWMPin, TURNING_SPEED);
    delay(TURNING_DELAY / 2);
    analogWrite(steerPWMPin, 0);
  } else {
    // turn to the right completely
    digitalWrite(steerDirPin, HIGH);
    analogWrite(steerPWMPin, TURNING_SPEED);
    delay(TURNING_DELAY);
  
    // align steering
    digitalWrite(steerDirPin, LOW);
    analogWrite(steerPWMPin, TURNING_SPEED);
    delay(TURNING_DELAY / 2);
    analogWrite(steerPWMPin, 0);  
  }

  currentSteerDir = STRAIGHT;
}

int getUSDistance(int echoPin) {
  long duration;
  int cms;

  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);

  duration = pulseIn(echoPin, HIGH);

  cms = duration / 29 / 2;
  return cms;
}

void setLedHigh(int ledPin) {
  digitalWrite(ledLeft, LOW);
  digitalWrite(ledCenter, LOW);
  digitalWrite(ledRight, LOW);
  
  digitalWrite(ledPin, HIGH);
}

